#!/bin/bash
read -r -d '' CA <<EOL
[dn]
CN=localhost

[req]
distinguished_name=dn

[EXT]
basicConstraints=CA:TRUE
keyUsage=digitalSignature, keyAgreement
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always
subjectAltName=DNS:localhost
extendedKeyUsage=serverAuth
EOL

openssl req -x509 -out localhost.crt -keyout localhost.key -newkey rsa:2048 -nodes -sha256 -subj '/CN=localhost' -extensions EXT -config <( printf "$CA")
