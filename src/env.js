const DEBUG = process.env.DEBUG === 'true';
const SENTRY_DSN = process.env.SENTRY_DSN;
const ENVIRONMENT = process.env.NODE_ENV;
const RELEASE = process.env.COMMIT_REF;

const appSettings = {
	GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID,
	GOOGLE_OPTIMIZE_ID: process.env.GOOGLE_OPTIMIZE_ID,
	DEBUG,
	SENTRY_DSN,
	ENVIRONMENT,
	RELEASE,
};

export { appSettings };
