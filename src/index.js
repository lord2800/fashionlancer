import Vue from 'vue';
import VueRouter from 'vue-router';
import VueAnalytics from 'vue-analytics';
import VueModal from 'vue-js-modal';

import Sentry from './app/sentry';

import buildRouter from './app/routes';
import App from './App';

import { appSettings } from './env';

Vue.config.performance = appSettings.DEBUG;
Vue.config.productionTip = false;

[ VueRouter, Sentry ].forEach(plugin => Vue.use(plugin));

const router = buildRouter();

Vue.use(VueModal, {
	dynamic: true,
	injectModalsContainer: true
});

Vue.use(VueAnalytics, {
	id: appSettings.GOOGLE_ANALYTICS_ID,
	router,
	ignoreRoutes: ['/oauth/callback'],
	debug: {
		enabled: appSettings.DEBUG,
		sendHitTrack: !appSettings.DEBUG
	},
	autoTracking: {
		exception: false,
		pageviewTemplate: (route) => route.matched[0] && route.matched[0].path || ''
	},
	require: [appSettings.GOOGLE_OPTIMIZE_ID],
	set: [{ field: 'anonymizeIp', value: true }]
});

document.addEventListener('DOMContentLoaded', () => {
	new Vue({ router, render: h => h(App) }).$mount('#app');
});
