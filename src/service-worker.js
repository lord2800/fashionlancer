/* eslint-env serviceworker */
import {
	populateCache,
	cleanCaches,
	normalRequest,
	rangeRequest,
	uncachedRequest,
	apiRequest,
	isUncacheableUrl,
	API_PREFIX
} from './lib';

import { apiSettings } from './env';
import * as Sentry from '@sentry/browser';

function fillAppCache() {
	return populateCache(apiSettings.APP_CACHE_NAME, fetch('/parcel-manifest.json').then(res => res.json()));
}

self.addEventListener('error', event => {
	if(apiSettings.SENTRY_DSN) {
		Sentry.init({
			dsn: apiSettings.SENTRY_DSN,
			debug: apiSettings.DEBUG,
			environment: apiSettings.ENVIRONMENT,
			release: apiSettings.RELEASE,
			integrations: [ ]
		});
		Sentry.captureException(event.error);
	}
});

self.addEventListener('install', event => {
	const queue = [];
	if(!apiSettings.DEBUG) {
		queue.push(fillAppCache());
	}
	event.waitUntil(Promise.all(queue).then(() => console.debug('Finished waiting for install')));
});

self.addEventListener('activate', event => {
	const queue = [self.clients.claim()];
	if(!apiSettings.DEBUG) {
		queue.push(cleanCaches([ apiSettings.APP_CACHE_NAME, apiSettings.STATIC_CACHE_NAME ]));
	}
	event.waitUntil(Promise.all(queue).then(() => console.debug('Finished waiting for activate')));
});

self.addEventListener('message', event => {
	switch(event.data.message) {
		case 'skipWaiting':
			console.debug('Skipping the wait!');
			self.skipWaiting();
			event.source.postMessage({ message: 'skipped' });
			break;
		case 'cachebust':
			console.debug('Clearing all caches...');
			Promise.all([
				cleanCaches(),
				fillAppCache()
			]).then(() => {
				console.debug('Done');
				event.source.postMessage({ message: 'cleared' });
			});
			break;
	}
});

self.addEventListener('fetch', event => {
	const url = new URL(event.request.url);

	if(url.hostname === 'sentry.io') {
		console.debug('Disabling cache for sentry request');
		return event.respondWith(uncachedRequest(event.request));
	}

	if(url.pathname.startsWith(API_PREFIX)) {
		console.debug('Responding with API request for', event.request.url);
		return event.respondWith(apiRequest(event.request));
	}

	if(apiSettings.DEBUG || (event.request.method !== 'GET' || isUncacheableUrl(url))) {
		console.debug('Responding with uncached request for url', event.request.url);
		return event.respondWith(uncachedRequest(event.request));
	}

	if(event.request.headers.has('range')) {
		console.debug('Responding with range request for url', event.request.url);
		return event.respondWith(rangeRequest(event.request, apiSettings.STATIC_CACHE_NAME));
	}

	console.debug('Responding with possibly cached request for url', event.request.url);
	return event.respondWith(normalRequest(event.request, apiSettings.STATIC_CACHE_NAME));
});
