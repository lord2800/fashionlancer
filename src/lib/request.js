function uncachedRequest(request) {
	return fetch(request, { cache: 'no-store' });
}

function normalRequest(request, cacheName) {
	return caches.match(request)
		.then(response => {
			if(response) {
				console.debug('Cache-matched request for url', request.url, 'in cache', cacheName);
				return response;
			}

			return uncachedRequest(request).then(response => {
				const clone = response.clone();

				console.debug('Caching response for', request.url, 'in cache', cacheName);
				return caches.open(cacheName)
					.then(cache => cache.put(request, clone))
					.then(() => response);
			});
		});
}

function rangeRequest(request, cacheName) {
	const rangeHeader = request.headers.get('range');

	const clone = request.clone();
	clone.headers.delete('range');

	return normalRequest(clone, cacheName)
		.then(response => response.arrayBuffer())
		.then(buffer => {
			const [type, range] = rangeHeader.split('=');

			if(type !== 'bytes' || range.indexOf('-') === -1) {
				return rangeNotSatisfiable(buffer.byteLength);
			}

			let [start, end] = range.split('-');
			if(end === '') {
				end = buffer.length - 1;
			}

			const bytes = buffer.slice(Number(start), Number(end) + 1);
			return rangeResponse(bytes, start, end, buffer.byteLength);
		});
}

function rangeResponse(bytes, start, end, length) {
	return new Response(bytes, {
		status: 206,
		statusText: 'Partial Content',
		headers: { 'Content-Range': `bytes ${start}-${end}/${length}` }
	});
}

function rangeNotSatisfiable(length) {
	return new Response(null, {
		status: 416,
		statusText: 'Range Not Satisfiable',
		headers: { 'Content-Range': `bytes */${length}` }
	});
}

export { normalRequest, rangeRequest, uncachedRequest };
