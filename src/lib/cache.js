function populateCache(cacheName, manifest) {
	console.debug('Populating cache', cacheName);
	return Promise.all([
		caches.open(cacheName),
		manifest
	]).then(([cache, files]) => {
		console.debug('Caching files', Object.values(files));
		return cache.addAll(Object.values(files));
	});
}

function cleanCaches(keepNames = []) {
	console.debug('Clearing all caches except', keepNames);
	return caches.keys().then(keys =>
		Promise.all(
			keys
				.filter(key => !keepNames.includes(key))
				.map(key => caches.delete(key))
		)
	);
}

export { populateCache, cleanCaches };
