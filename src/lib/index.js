function isUncacheableUrl(url) {
	return !url.protocol.startsWith('http') ||
		url.hostname === 'www.google-analytics.com';
}

export { isUncacheableUrl };
export * from './cache';
export * from './request';
