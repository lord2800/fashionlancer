import VueRouter from 'vue-router';

function buildRouter() {
	const router = new VueRouter({
		mode: 'history',
		routes: [
		]
	});

	return router;
}

export default buildRouter;
