import * as Sentry from '@sentry/browser';
import { appSettings } from '../env';

export default class SentryPlugin {
	static install(Vue) {
		if(SentryPlugin.install.installed) { return; }
		SentryPlugin.install.installed = true;
		if(appSettings.SENTRY_DSN) {
			Sentry.init({
				dsn: appSettings.SENTRY_DSN,
				debug: appSettings.DEBUG,
				environment: appSettings.ENVIRONMENT,
				release: appSettings.RELEASE,
				integrations: [ new Sentry.Integrations.Vue({ Vue }) ]
			});
		}
	}
}
